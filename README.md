# flask-aws
This is a Flask application that accepts JSON requests to store data in AWS DynamoDB.<br>
This application is automatically deployed using Gitlab CI/CD to AWS EBS.

## Getting started
### To run locally

#### Pre-requisites
- python3 (preferably >3.6)
- pip 
- aws cli (installed and configured)

#### Installation steps 
1. Download repo

```bash
git clone https://gitlab.com/m3rryqold/flask-aws.git && cd flask-aws
```
2. Install requirements
```bash
pip install -r requirements.txt
```
3. Create `.env` file in root directory to store AWS access configuration.
```bash
touch .env
```
```
AWS_ACCESS_KEY_ID='your_access_key_id'
AWS_SECRET_ACCESS_KEY='your_secret_access_key'
REGION_NAME='us-east-1'
```
4. Create DynamoDB table
```bash
aws dynamodb create-table --cli-input-json file://create-table.json 
```
5. Start the app
```bash
python3 app.py
```
6. Visit the root URL<br>
By default, this is [127.0.0.1:5000](http://127.0.0.1:5000) <br>
This fetches all the data stored in your DynamoDB table

7. Send a create request to the create API [127.0.0.1:5000/create-movies](http://127.0.0.1:5000/create-movies)
<br>Sample data:
```
[
    {
        "MovieTitle": "Avengers",
        "FavoriteCharacter": "Thor"
    },
    {
        "MovieTitle": "Hulk",
        "FavoriteCharacter": "Hulk"
    }
]
```
<br>Check [127.0.0.1:5000](http://127.0.0.1:5000) again

### To run live
1. Configure variables (Settings->CI/CD->Variables)<br>
same variables in line 28- line 30 <br>
<br>
Currently, A Gitlab CICD pipeline has already been setup for end to end deployment.<br>
All that is required is a push action. Typically:<br>
push->test->deploy


### To do
- IAC with CloudFormation
- CD with CodePipeline and SAM
