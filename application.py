from flask import Flask, request

application = Flask(__name__)

import dynamodb_controller as dynamodb


@application.route('/')
def root_route():
    response = dynamodb.listMovies()
    return response

#  Add movies
#  Route: http://localhost:5000/create-movies
#  Method : POST
@application.route('/create-movies', methods=['POST'])
def createMovies():
    print('here')
    # data = request.get_json()
    data = request.get_data()
    # print(data)
    response = dynamodb.createMovies(data)    
    
    if (response['ResponseMetadata']['HTTPStatusCode'] == 200):
        return {
            'msg': 'Added successfully',
        }

    return {  
        'msg': 'Some error occcured',
        'response': response
    }

if __name__ == '__main__':
    application.run(port=5000, debug=True)
