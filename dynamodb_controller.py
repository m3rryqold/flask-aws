import json
import boto3
from dotenv import load_dotenv
import os

load_dotenv()

AWS_ACCESS_KEY_ID     = os.getenv("AWS_ACCESS_KEY_ID")
AWS_SECRET_ACCESS_KEY = os.getenv("AWS_SECRET_ACCESS_KEY")
AWS_DEFAULT_REGION    = os.getenv("AWS_DEFAULT_REGION")
print(AWS_DEFAULT_REGION)

# set client and resource
client = boto3.client(
    'dynamodb',
    aws_access_key_id     = AWS_ACCESS_KEY_ID,
    aws_secret_access_key = AWS_SECRET_ACCESS_KEY,
    region_name           = "us-east-1",
)
resource = boto3.resource(
    'dynamodb',
    aws_access_key_id     = AWS_ACCESS_KEY_ID,
    aws_secret_access_key = AWS_SECRET_ACCESS_KEY,
    region_name           = "us-east-1",
)

table = resource.Table('MoviesTable')

# create data from JSON 
def createMovies(movies_json):
    movies = movies_json.decode()
    print(movies)
    response=[]
    # with open('items.json') as json_data:
    items = json.loads(movies)
    print(type(items))

    with table.batch_writer() as batch:

        # Loop through the JSON objects
        for item in items:
            put_response = batch.put_item(Item=item)
            response.append(put_response)
    return response

def createMoviesFromFile(movies_json):
    print(movies_json)
    response=[]
    # with open('items.json') as json_data:
    items = json.load(movies_json)
    print(items)

    with table.batch_writer() as batch:

        # Loop through the JSON objects
        for item in items:
            print(item)
            print(type(item))
            put_response = batch.put_item(Item=item)
            response.append(put_response)
    return response

def listMovies():
    response = table.scan()
    data = response['Items']
    return data

# with open('movies.json') as json_data:
#     print(json_data)
#     print(type(json_data))
#     createMoviesFromFile(json_data)
